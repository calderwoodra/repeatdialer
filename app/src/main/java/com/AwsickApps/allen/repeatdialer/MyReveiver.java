package com.AwsickApps.allen.repeatdialer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

/**
 * Created by Allen on 7/28/2014.
 */
public class MyReveiver extends BroadcastReceiver {

    private final int BACK_FROM_DIAL = 10;

    @Override
    public void onReceive(Context context, Intent intent) {
        endCall(context);
        Intent oldIntent = new Intent(context, HomeScreen.class);
        oldIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        oldIntent.putExtra("backFromDial", BACK_FROM_DIAL);
        context.startActivity(oldIntent);
    }

    public void endCall(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object telephonyService = m.invoke(tm);

            c = Class.forName(telephonyService.getClass().getName());
            m = c.getDeclaredMethod("endCall");
            m.setAccessible(true);
            m.invoke(telephonyService);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
