package com.AwsickApps.allen.repeatdialer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

/**
 * Created by allen on 1/21/15.
 */
public class PhoneListener extends PhoneStateListener {

    Context context;

    public PhoneListener(Context context) {
        this.context = context;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        if(TelephonyManager.CALL_STATE_IDLE == state && SharedData.callPlaced) {
            //wait for phone to go offhook (probably set a boolean flag) so you know your app initiated the call.
            onReturn();

        }
    }

    public void onReturn(){
        long startTime = SharedData.startTime;
        long endTime = System.currentTimeMillis();

        //if call lasted <30 seconds
        //Send SMS, End Repeating Alarm
        if (((endTime - startTime) < 30000)) {
            if(!SharedData.textMessageDisabled) {
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(SharedData.phoneNumber, null, SharedData.textMessage, null, null);
                Toast.makeText(context, "User Answered, message sent.", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(context, "User Answered.", Toast.LENGTH_LONG).show();
            }
            //if call lasted 30+ seconds
            //redial
        } else if(SharedData.dials > 1) {
            SharedData.dials--;
            startDial();

            //if no answer after specified number of dials
            //Post no response, End Repeating Alarm
        }else{
            SharedData.dials--;
            SharedData.callPlaced = false;
            Toast.makeText(context, "User did not Answer.", Toast.LENGTH_LONG).show();
        }
    }

    public void startDial(){

        String phoneNumber = SharedData.phoneNumber;

        //performs call
        if (!phoneNumber.equals("")) {

            Intent intent = new Intent(context, MyReveiver.class);
            PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 32000, pi);

            Uri number = Uri.parse("tel:" + phoneNumber);
            Intent dial = new Intent(Intent.ACTION_CALL, number);

            SharedData.startTime = System.currentTimeMillis();
            context.startActivity(dial);
        }
    }

}
