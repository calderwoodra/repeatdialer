package com.AwsickApps.allen.repeatdialer;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Allen on 7/22/2014.
 */
public class Splash extends Activity{

    Bundle basket;
    ArrayList<String> names, IDs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);

        Thread load = new Thread(){
            public void run(){
                loadHomeScreen();
            }
        };
        load.start();

    }



    public void loadHomeScreen(){

        HashMap<String, ArrayList<String>> contacts = readContacts();
        names = contacts.get("Names");
        IDs = contacts.get("IDs");

        Intent ourIntent = new Intent(Splash.this, HomeScreen.class);
        basket = new Bundle();
        basket.putStringArrayList("Names", names);
        basket.putStringArrayList("IDs", IDs);

        ourIntent.putExtras(basket);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        startActivity(ourIntent);
        finish();
    }

    public HashMap<String, ArrayList<String>> readContacts(){

        tree contactsTree = new tree();
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        Uri QUERY_URI = ContactsContract.Contacts.CONTENT_URI;


        ContentResolver cr = getContentResolver();
        final String[] projection = {DISPLAY_NAME, HAS_PHONE_NUMBER};
        Cursor cur = cr.query(QUERY_URI, projection, HAS_PHONE_NUMBER + " = ?", new String[]{"1"}, null);

        if (cur.getCount() > 0) {

            while (cur.moveToNext()) {
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                System.out.println("name : " + name);
                contactsTree.addNode(new node(name));
            }
        }

        cur.close();
        return contactsTree.inOrder();
    }
}

class node{
    node leftNode;
    node rightNode;
    String name;
    String number;

    public node(String name){
        this.name = name;
        leftNode = null;
        rightNode = null;
    }
}

class tree{
    node rootNode;
    node currentNode;
    node tempNode;
    boolean right;


    ArrayList<String> names;
    ArrayList<String> phone;

    public tree(){
        rootNode = null;
        names = new ArrayList<String>();
        phone = new ArrayList<String>();
    }

    public void addNode(node newNode){
        if(rootNode == null){
            rootNode = newNode;

        }else {
            currentNode = rootNode;

            while (currentNode != null) {
                tempNode = currentNode;
                right = newNode.name.compareTo(currentNode.name) > 0;
                if (right) {
                    currentNode = currentNode.rightNode;
                } else {
                    currentNode = currentNode.leftNode;
                }
            }

            if (right)
                tempNode.rightNode = newNode;
            else
                tempNode.leftNode = newNode;
        }
    }

    public HashMap<String, ArrayList<String>> inOrder(){

        ioTraversal(rootNode);
        HashMap<String, ArrayList<String>> ret = new HashMap<String, ArrayList<String>>();
        ret.put("Names", names);
        ret.put("IDs", phone);
        return ret;
    }

    private void ioTraversal(node root){


        if(root == null) return;

        ioTraversal( root.leftNode );

        names.add(root.name);
        phone.add(root.number);

        ioTraversal( root.rightNode );
    }
}
