package com.AwsickApps.allen.repeatdialer;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Allen on 7/18/2014.
 */
public class ContactsList extends ListActivity implements AdapterView.OnItemClickListener{

    private ArrayList<String> Names, filteredList;
    private int filterSize;
    private String[] NamesArray;
    private ArrayList<String> IDs;
    private String ID = null;
    private Bundle basket;
    private Intent intent, returnIntent;
    private Adapter listAdapter;
    private EditText etContactFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();

        //setListAdapter(new ArrayAdapter<String>(ContactsList.this, android.R.layout.simple_list_item_1, NamesArray));

    }

    /*protected void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);

        ArrayList<String> phoneNumbers = new ArrayList<String>();
        String image_uri = null;

        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                                    ContactsContract.Contacts._ID, ContactsContract.CommonDataKinds.Phone.PHOTO_URI};

        //Retrieves Phone Numbers
        Cursor cr = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection, ContactsContract.Contacts.DISPLAY_NAME + " = ?", new String[]{Names.get(position)}, null);

        while(cr.moveToNext()){
            image_uri = cr.getString(cr.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
            phoneNumbers.add(cr.getString(cr.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            if(ID == null)
                ID = cr.getString(cr.getColumnIndex(ContactsContract.Contacts._ID));
        }



        returnIntent.putExtra("name", Names.get(position));
        returnIntent.putExtra("photo", image_uri);

        if(phoneNumbers.size() > 1){
            Intent queryNumber = new Intent(ContactsList.this, NumberConfirmation.class);
            queryNumber.putExtra("PHONE_NUMBERS",phoneNumbers);
            startActivityForResult(queryNumber, 0);
        }else{
            setResult(basket.getInt("selected"), returnIntent);
            returnIntent.putExtra("PHONE_NUMBER", phoneNumbers.get(0));
            returnIntent.putExtra("ID", ID);
            finish();
        }


    }*/

    public void init(){
        intent = getIntent();
        basket = intent.getExtras();

        returnIntent = new Intent(ContactsList.this, HomeScreen.class);
        setResult(basket.getInt("not_selected"), returnIntent);

        Names = basket.getStringArrayList("Names");
        filteredList = Names;
        filterSize = Names.size();
        IDs = basket.getStringArrayList("IDs");

        setContentView(R.layout.contacts_list);
        listAdapter = new Adapter(this, Names);
        getListView().setAdapter(listAdapter);
        getListView().setOnItemClickListener(this);

        etContactFilter = (EditText) findViewById(R.id.etFilter);
        etContactFilter.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                Log.d("text_changed", "text changed: " + charSequence.toString());


            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = etContactFilter.getText().toString();

                //if (text.length() < filterSize)
                filteredList = new ArrayList<String>();

                //filterSize = text.length();

                for(int j = 0; j < Names.size(); j++){
                    if(Names.get(j).toLowerCase().contains(text.toLowerCase()))
                        filteredList.add(Names.get(j));

                }

                listAdapter.setContacts(filteredList);
            }
        });

        //NamesArray = new String[Names.size()];

        //for(int i = 0; i < Names.size(); i++){
            //NamesArray[i] = Names.get(i);
        //}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 1){
            Bundle basket2 = data.getExtras();
            returnIntent.putExtra("PHONE_NUMBER", basket2.getString("PHONE_NUMBER"));
            returnIntent.putExtra("ID", ID);

            setResult(basket.getInt("selected"), returnIntent);
            finish();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ArrayList<String> phoneNumbers = new ArrayList<String>();
        String image_uri = null;

        String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Contacts._ID, ContactsContract.CommonDataKinds.Phone.PHOTO_URI};

        //Retrieves Phone Numbers
        Cursor cr = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection, ContactsContract.Contacts.DISPLAY_NAME + " = ?", new String[]{filteredList.get(i)}, null);

        while(cr.moveToNext()){
            image_uri = cr.getString(cr.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
            phoneNumbers.add(cr.getString(cr.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
            if(ID == null)
                ID = cr.getString(cr.getColumnIndex(ContactsContract.Contacts._ID));
        }



        returnIntent.putExtra("name", filteredList.get(i));
        returnIntent.putExtra("photo", image_uri);

        if(phoneNumbers.size() > 1){
            Intent queryNumber = new Intent(ContactsList.this, NumberConfirmation.class);
            queryNumber.putExtra("PHONE_NUMBERS",phoneNumbers);
            startActivityForResult(queryNumber, 0);
        }else{
            setResult(basket.getInt("selected"), returnIntent);
            returnIntent.putExtra("PHONE_NUMBER", phoneNumbers.get(0));
            returnIntent.putExtra("ID", ID);
            finish();
        }
    }

    private class Adapter extends BaseAdapter{

        private List<String> contacts;
        private Context context;
        private LayoutInflater layoutInflater;

        public Adapter(Context context, List<String> contacts){
            this.context = context;
            this.contacts = contacts;
            layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return contacts.size();
        }

        @Override
        public Object getItem(int i) {
            return contacts.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View row = layoutInflater.inflate(R.layout.row_contact, viewGroup, false);
            ((TextView) row.findViewById(R.id.tvContact)).setText(contacts.get(i));
            return row;
        }

        public void setContacts(List<String> contacts){
            this.contacts = contacts;
            this.notifyDataSetChanged();
        }
    }
}