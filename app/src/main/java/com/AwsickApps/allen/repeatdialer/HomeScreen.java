package com.AwsickApps.allen.repeatdialer;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Telephony;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.ArrayList;


public class HomeScreen extends Activity implements OnClickListener {

    private Button bStartDial;
    private ImageButton bContact;
    private EditText etMessage;
    private TextView tvContact, tvPhoneNumber;
    private NumberPicker npRepeats;
    private Bundle basket, contacts;
    private SharedPreferences sp;
    private ImageView ivContactPhoto;

    private ArrayList<String> Names;
    private ArrayList<String> IDs;

    private String phoneNumber, ID;
    private boolean backFromDial = false;

    private final int SETTINGS = 4;
    private final int DIAL_COMMENCED = 5;
    private final int CONTACT_SELECTED = 6;
    private final int CONTACT_NOT_SELECTED = 7;
    private final int BACK_FROM_DIAL = 10;
    private final int NUMBER_PICKER_MIN = 0;
    private final int NUMBER_PICKER_MAX = 20;
    private final int NUMBER_PICKER_VAL = 10;

    //*******************************************************
    //*						On Create						*
    //*******************************************************
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_home_screen);
        init();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(SharedData.callPlaced) {
            npRepeats.setValue(--SharedData.dials);
            SharedData.dials = 0;
            SharedData.callPlaced = false;
        }
    }

    public void init(){

        bContact = (ImageButton) findViewById(R.id.bContact);
        bStartDial = (Button) findViewById(R.id.bStartDial);
        etMessage = (EditText) findViewById(R.id.etMessage);
        tvContact = (TextView) findViewById(R.id.tvContact);
        tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
        npRepeats = (NumberPicker) findViewById(R.id.npRepeats);
        setNumberPickerTextColor(npRepeats, -1);
        ivContactPhoto = (ImageView) findViewById(R.id.ivContactPhoto);
        basket = new Bundle();
        contacts = getIntent().getExtras();

        sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedData.dials = 0;

        tvContact.setOnClickListener(this);
        bContact.setOnClickListener(this);
        bStartDial.setOnClickListener(this);

        npRepeats.setMinValue(NUMBER_PICKER_MIN);
        npRepeats.setMaxValue(NUMBER_PICKER_MAX);
        npRepeats.setValue(NUMBER_PICKER_VAL);

        SharedData.textMessageDisabled = sp.getBoolean("cbTextMessage", false);
        Names = contacts.getStringArrayList("Names");
        IDs = contacts.getStringArrayList("IDs");

        PhoneListener listener = new PhoneListener(this);
        TelephonyManager tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        tm.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public void onClick(View arg0){
        switch (arg0.getId()){

            case R.id.bContact:
               displayContacts(Names, IDs);
               break;

            case R.id.bStartDial:
                //begin dialing process
                if(!SharedData.textMessageDisabled){
                    if(etMessage.getText().toString().matches("")){
                        Toast.makeText(getBaseContext(), "Input Message to be Sent!", Toast.LENGTH_LONG).show();
                    }else if(tvContact.getText().toString().matches("No Contact Selected")){
                        Toast.makeText(getBaseContext(), "Select a contact from above!", Toast.LENGTH_LONG).show();
                    }else {
                        SharedData.textMessage = etMessage.getText().toString();
                        startDial();
                    }
                }else{
                    if(tvContact.getText().toString().matches("No Contact Selected")){
                        Toast.makeText(getBaseContext(), "Select a contact from above!", Toast.LENGTH_LONG).show();
                    }else {
                        startDial();
                    }
                }
                break;

            default:
                break;

        }
    }

    /*      This method gets our list of contacts from our intent, then passes them along
    * to a new activity which displays them for choosing so that the user can select
    * which contact they wish to call.
    */
    public void displayContacts(ArrayList<String> names, ArrayList<String> IDs){

        Intent ourIntent = new Intent(HomeScreen.this, ContactsList.class);
        basket.putStringArrayList("Names", names);
        basket.putStringArrayList("IDs", IDs);
        basket.putInt("selected", CONTACT_SELECTED);
        basket.putInt("not_selected", CONTACT_NOT_SELECTED);

        ourIntent.putExtras(basket);
        startActivityForResult(ourIntent, CONTACT_SELECTED);
    }


    /* This method is responsible for placing the phone call and setting the alarm */
    public void startDial(){

        SharedData.phoneNumber = phoneNumber;
        SharedData.callPlaced = true;
        SharedData.dials = npRepeats.getValue();

        //performs call
        if (!phoneNumber.equals("")) {

            Intent intent = new Intent(this, MyReveiver.class);
            PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 32000, pi);

            Uri number = Uri.parse("tel:" + phoneNumber);
            Intent dial = new Intent(Intent.ACTION_CALL, number);

            SharedData.startTime = System.currentTimeMillis();
            startActivity(dial);
        }
    }

    public static boolean setNumberPickerTextColor(NumberPicker numberPicker, int color)
    {
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText){
                try{
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText)child).setTextColor(color);
                    numberPicker.invalidate();
                    return true;
                }
                catch(NoSuchFieldException e){
                }
                catch(IllegalAccessException e){
                }
                catch(IllegalArgumentException e){
                }
            }
        }
        return false;
    }

    /*      On return from selecting a contact, sets the EditText to the contacts name and number.
     *  If the activity returns from anything other than the contact being selected (ie. phone call),
     *  set backFromDial = true.
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("TAG", "in activity result, RESULT_CODE: " + resultCode);

        if(resultCode == CONTACT_SELECTED) {
            Bundle bag = data.getExtras();

            String name = bag.getString("name");
            phoneNumber = bag.getString("PHONE_NUMBER");
            ID = bag.getString("ID");

            try{
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(bag.getString("photo")));
                ivContactPhoto.setImageBitmap(bitmap);
            }catch(Exception e){
                Drawable drawable = getResources().getDrawable(R.drawable.ic_launcher);
                ivContactPhoto.setImageDrawable(drawable);
            }

            tvContact.setText(name);
            tvPhoneNumber.setText(phoneNumber);
            backFromDial = false;

        }else if(resultCode == SETTINGS) {
            SharedData.textMessageDisabled = sp.getBoolean("cbTextMessage", false);
            backFromDial = false;

        }else if(resultCode == CONTACT_NOT_SELECTED) {
            backFromDial = false;


        }
        //going to call on return when the alarm sends a broadcast.
        /*else if(resultCode == DIAL_COMMENCED){
            Log.d("TAG", "DIAL_COMMENCED");
            backFromDial = true;
            onReturn();

        }else{
            backFromDial = true;
            //when progaurd is set to true, the result code is 0
            if(getIntent().getExtras().getInt("code") == BACK_FROM_DIAL)
                onReturn();
        }*/
    }


    //*******************************************************************
    //*						      On Return 	    					*
    //*******************************************************************
    /*  On restart of the activity, check if activity was back from call,
     *      if back from call, and call lasted 30+ seconds, redial
     *      if back from call, and call lasted <30 seconds, send SMS
     *      if not back from call, ie. reopening the app, do nothing.
     */
    /*public void onReturn(){
        //if back from call
        if((!tvContact.getText().toString().matches("No Contact Selected")) && backFromDial) {
            endTime = System.currentTimeMillis();

            //if call lasted <30 seconds
            //Send SMS, End Repeating Alarm
            if (((endTime - startTime) < 30000)) {
                if(!textMessageDisabled) {
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(phoneNumber, null, etMessage.getText().toString(), null, null);
                    Toast.makeText(getBaseContext(), "User Answered, message sent.", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getBaseContext(), "User Answered.", Toast.LENGTH_LONG).show();
                }
                //if call lasted 30+ seconds
                //redial
            } else if(npRepeats.getValue() > 1) {
                npRepeats.setValue(npRepeats.getValue() - 1);
                startDial();

                //if no answer after specified number of dials
                //Post no response, End Repeating Alarm
            }else{
                npRepeats.setValue(npRepeats.getValue() - 1);
                Toast.makeText(getBaseContext(), "User did not Answer.", Toast.LENGTH_LONG).show();
            }
        }

        backFromDial = false;
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){
            case R.id.action_settings:
                Intent i = new Intent("android.intent.action.SETTINGS");
                i.putExtra("result_code", SETTINGS);
                startActivityForResult(i, SETTINGS);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}