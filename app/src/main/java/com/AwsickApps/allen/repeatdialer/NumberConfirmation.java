package com.AwsickApps.allen.repeatdialer;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Allen on 8/1/2014.
 */
public class NumberConfirmation extends ListActivity {

    Bundle basket;
    ArrayList<String> Numbers;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Numbers));

    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent returnIntent = new Intent(this, ContactsList.class);
        returnIntent.putExtra("PHONE_NUMBER", Numbers.get(position));
        setResult(1, returnIntent);
        finish();
    }

    public void init(){
        basket = getIntent().getExtras();
        Numbers = basket.getStringArrayList("PHONE_NUMBERS");
    }
}
