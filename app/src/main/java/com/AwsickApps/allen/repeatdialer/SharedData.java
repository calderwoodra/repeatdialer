package com.AwsickApps.allen.repeatdialer;

/**
 * Created by allen on 1/21/15.
 */
public class SharedData {

    public static int dials = 0;
    public static long startTime;
    public static boolean callPlaced = false;
    public static boolean textMessageDisabled;
    public static String textMessage = "";
    public static String phoneNumber = "";

}
