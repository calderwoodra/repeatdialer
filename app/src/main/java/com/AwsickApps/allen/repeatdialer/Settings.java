package com.AwsickApps.allen.repeatdialer;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Allen on 7/28/2014.
 */
public class Settings extends PreferenceActivity {

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        Intent returnIntent = new Intent(Settings.this, HomeScreen.class);
        setResult(getIntent().getExtras().getInt("result_code"), returnIntent);
    }
}
